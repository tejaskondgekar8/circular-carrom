using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class WallOpeningCircular : MonoBehaviour
{
    [SerializeField]
    float IncrementAngle = 16f;
    [SerializeField]
    WallOpeningBlock ob;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        DrawBlocks();
    }

    public void DrawBlocks()
    {
        if(TotalBlockRequired() == CountOpeningBlocks())
        {
            return;
        }
        if(IncrementAngle <= 0.5f)
        {
            IncrementAngle = 0.5f;
        }
        DeleteAllBlocks();
        while ((TotalBlockRequired() - CountOpeningBlocks()) > 0)
        {
            WallOpeningBlock wob = PrefabUtility.InstantiatePrefab(ob) as WallOpeningBlock;
            wob.transform.SetParent(gameObject.transform);
            wob.transform.SetPositionAndRotation(
                wob.transform.position,
                Quaternion.Euler(
                    wob.transform.rotation.eulerAngles.x,
                    IncrementAngle * CountOpeningBlocks(),
                    wob.transform.rotation.eulerAngles.z
                )
            );
        }
    }

    void DeleteAllBlocks()
    {
        foreach(GameObject wob in GetOpeningBlocks())
        {
            GameObject.DestroyImmediate(wob);
        }
    }

    public int CountOpeningBlocks()
    {
        return GetOpeningBlocks().Length;
    }

    GameObject[] GetOpeningBlocks()
    {
        return GameObject.FindGameObjectsWithTag("Opening_Block");
    }

    public int TotalBlockRequired()
    {
        return (int)Mathf.Ceil((360f / IncrementAngle));
    }

    public float ActualAngle()
    {
        return 360f / TotalBlockRequired();
    }


}
