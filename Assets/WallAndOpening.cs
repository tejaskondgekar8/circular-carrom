using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallAndOpening : MonoBehaviour
{
    [SerializeField]
    bool isRotating = false;
    [SerializeField]
    float rotationSpeed = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isRotating)
        {
            transform.rotation = Quaternion.Euler(
                transform.rotation.eulerAngles.x,
                transform.rotation.eulerAngles.y + Time.deltaTime * rotationSpeed,
                transform.rotation.eulerAngles.z
            );
        }

    }
}
