using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WallOpeningCircular))]

#if UNITY_EDITOR
public class WallOpeningEditor : Editor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        WallOpeningCircular woc = (WallOpeningCircular)target;
        EditorGUILayout.LabelField("Count of blocks ", woc.CountOpeningBlocks().ToString());
        EditorGUILayout.LabelField("Total Blocks Required ", woc.TotalBlockRequired().ToString());
        EditorGUILayout.LabelField("Actual angle will be ", woc.ActualAngle().ToString());
    }

}
#endif
