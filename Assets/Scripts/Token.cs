using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token : MonoBehaviour
{
    bool ToBeDestroyed = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "OpeningObjectTag")
        {
            if(ToBeDestroyed){
                return;
            }
            ToBeDestroyed = true;
            GameManager gm = FindObjectOfType<GameManager>();
            gm.DecrementTokens();
            Score.scoreValue += 10;
            gm.LevelComplete();
            /*ButtonManager bM = FindObjectOfType<ButtonManager>();
            bM.gameObject.transform.localPosition = new Vector3(0, -127f, 0);*/
            
            Destroy(gameObject, 1f);
        }
    }
}
