using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Striker : MonoBehaviour
{
    Plane horizontalPlane = new Plane(Vector3.up, new Vector3(0,1,0)) ;
    bool lineDraw = false;
    LineRenderer lr;
    Camera thisCamera;
    Vector3 mouseWorld;
    [SerializeField]
    float circleRadius = 1f;

    //ButtonManager bM = FindObjectOfType<ButtonManager>();

    private void Start()
    {
        thisCamera = Camera.main;
        lr = FindObjectOfType<StrikerLine>().GetComponent<LineRenderer>();
        StartCoroutine(RotateCamera());
        /*ButtonManager bM = FindObjectOfType<ButtonManager>();
        bM.enabled = false;*/
    }

    private void Update()
    {
        if (lineDraw)
        {
            DrawLine();
        }
        if (Input.GetMouseButtonDown(0))
        {
            SimulateMouseDown();
        }
        if (Input.GetMouseButtonUp(0))
        {
            SimulateOnMouseUp();
        }

    }

    private void SimulateOnMouseUp()
    {
        float power = Vector3.Distance(mouseWorld, transform.position);
        GetComponent<Rigidbody>().velocity =  - (mouseWorld - transform.position)* power;
        lineDraw = false;
        StartCoroutine(RotateCamera());
    }

    IEnumerator RotateCamera()
    {
        yield return new WaitForSecondsRealtime(2);
        ResetCamera();
    }

    void ResetCamera()
    {
        //CircularBoard cb = FindObjectOfType<CircularBoard>();
        Vector3 centerPoint = new Vector3(0, 0, 0);
        Vector3 strikerPoint = new Vector3(transform.position.x, 0, transform.position.z);
        Vector3 horizontalPoint = new Vector3(10, 0, 0);
        Vector3 difference = horizontalPoint - centerPoint;
        Vector3 differenceStriker = strikerPoint - centerPoint;

        float angle = Vector3.SignedAngle( difference, differenceStriker, Vector3.up) - 90;
        CameraPoint cp = FindObjectOfType<CameraPoint>();
        cp.setCameraRotation(angle);
        //cp.transform.SetPositionAndRotation(cp.transform.position, Quaternion.Euler(0, angle, 0));
        
        Vector3.Distance(differenceStriker, difference);
    }

    private void SimulateMouseDown()
    {
        lineDraw = true;
    }

    private void DrawLine()
    {
        
        lr.startWidth = 0.2f;
        lr.endWidth = 0.2f;
        lr.positionCount = 2;
        lr.SetPosition(0, transform.position);
        Vector3 mousePos = Input.mousePosition;
        var ray = thisCamera.ScreenPointToRay(mousePos);
        if (horizontalPlane.Raycast(ray, out float hit))
        {
            mouseWorld = ray.GetPoint(hit);
            Vector3 differenceFromCenter = transform.position - new Vector3(0, 0, 0);
            Vector3 mousePointNew = mouseWorld - differenceFromCenter;
            Vector3 newInverse = new Vector3(-mousePointNew.x, mousePointNew.y, -mousePointNew.z);
            Vector3 finalPosition = newInverse + differenceFromCenter;
            lr.SetPosition(1,finalPosition);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "OpeningObjectTag")
        {
            StartCoroutine(StartGameOverProcess());
        }
    }

    IEnumerator StartGameOverProcess()
    {
        yield return new WaitForSecondsRealtime(3);
        SceneManager.LoadScene(5);
    }

    IEnumerator ResetStrikerPosition(Vector3 newPosition)
    {
        GetComponent<Rigidbody>().isKinematic = true;
        yield return new WaitForSecondsRealtime(0.1f);
        /**
         * Lift up
         */
        Vector3 currentPosition = transform.position;
        Vector3 lifeToPosition = new Vector3(
            transform.position.x, transform.position.y + 1f, transform.position.z
        );
        for (var t = 0f; t < 1; t += Time.deltaTime / 0.1f)
        {
            transform.position = Vector3.Lerp(currentPosition, lifeToPosition, t);
            yield return null;
        }

        /**
         * Move to new position
         */
        Vector3 moveToPosition = new Vector3(
            newPosition.x, lifeToPosition.y, newPosition.z
        );
        for (var t = 0f; t < 1; t += Time.deltaTime / 0.1f)
        {
            transform.position = Vector3.Lerp(lifeToPosition, moveToPosition, t);
            yield return null;
        }


        /**
         * Drop in place
         */
        GetComponent<Rigidbody>().isKinematic = false;
        yield return null;
    }

    public void MoveToLinePosition()
    {
        Vector3 destnationPosition = LinePosition();
        Debug.Log(destnationPosition);
        StartCoroutine(ResetStrikerPosition(destnationPosition));
    }

    Vector3 LinePosition()
    {
        /**
         * Calculate angle
         */
        float angle = Mathf.Atan2(transform.position.z , transform.position.x);
        /**
         * Calculate LinePositionX
         */
        float x = circleRadius * Mathf.Cos(angle);

        /**
         * Calculate LinePositionY
         */
        float z = circleRadius * Mathf.Sin(angle);

        return new Vector3(x, transform.position.y, z);
    }
}
