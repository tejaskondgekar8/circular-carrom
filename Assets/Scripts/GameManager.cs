using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    int NumOfTokens = 0;
    // Start is called before the first frame update
    void Start()
    {        

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        Tokens ts = FindObjectOfType<Tokens>();
        Debug.Log("ts " + ts);
        Token[] tks = ts.GetComponentsInChildren<Token>();
        NumOfTokens = tks.Length;

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DecrementTokens()
    {
        NumOfTokens--;
    }
    public void LevelComplete()
    {
        if(NumOfTokens == 0)
        {
            Debug.Log("Level completed");
            StartCoroutine(FlyUpLevelComplete());
            ButtonManager bM = FindObjectOfType<ButtonManager>();
            bM.gameObject.transform.localPosition = new Vector3(0, -127f, 0);
        }
        else
        {
            Debug.Log(NumOfTokens+" Tokens remaining");
        }
    }

    IEnumerator FlyUpLevelComplete()
    {
        LevelCompleteText lcT = FindObjectOfType<LevelCompleteText>();
        RectTransform rt = lcT.GetComponent<RectTransform>();
        Backdrop bd = Resources.FindObjectsOfTypeAll<Backdrop>()[0];
        Debug.Log("backdrop " + bd);
        bd.gameObject.SetActive(true);
        for (var t = 0f; t < 1f; t += Time.deltaTime / 2f)
        {
            //lcT.gameObject.SetActive(false);
            rt.localPosition = Vector3.Lerp(rt.localPosition, new Vector3(0, 0, 0), Mathf.SmoothStep(0, 1, t));
            yield return null;
        }
    }
}
