using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class WallGenerator : MonoBehaviour
{
    [SerializeField]
    int NumberOfWallBlocks = 0;
    [SerializeField]
    float incrementAngle = 5f;


    [SerializeField]
    WallBlock wb;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        DrawWall();
    }

    WallBlock[] GetWallBlocks()
    {
        return GetComponentsInChildren<WallBlock>();
    }

    void DrawWall()
    {
        if (GetWallBlocks().Length == NumberOfWallBlocks)
        {
            return;
        }

        /**
         * Delete existing walls
         */
        if (GetWallBlocks().Length > 0)
        {
            foreach (WallBlock wb in GetWallBlocks())
            {
                DestroyImmediate(wb.gameObject);
            }
        }

        /**
         * Create new wall blocks
         */
        while (GetWallBlocks().Length < NumberOfWallBlocks && NumberOfWallBlocks != 0)
        {
            WallBlock _wb = PrefabUtility.InstantiatePrefab(wb) as WallBlock;
            _wb.transform.SetParent(gameObject.transform);
            _wb.transform.SetPositionAndRotation(
                _wb.transform.position,
                Quaternion.Euler(
                    _wb.transform.rotation.eulerAngles.x,
                    incrementAngle * GetWallBlocks().Length + gameObject.transform.rotation.eulerAngles.y,
                    _wb.transform.rotation.eulerAngles.z
                )
            );
        }

    }
}
