using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPoint : MonoBehaviour
{   
    [SerializeField]
    float RotationTime = 0.5f;

    public void setCameraRotation(float angle)
    {
        StartCoroutine(SetCameraRotationAnimated(angle));
    }
    IEnumerator SetCameraRotationAnimated(float angle) 
    {
        Quaternion fromAngle = transform.rotation;
        Quaternion toAngle = Quaternion.Euler(
            transform.rotation.eulerAngles.x,
            angle,
            transform.rotation.eulerAngles.z
        );
        for (var t = 0f; t < 1f; t += Time.deltaTime / RotationTime)
        {
            transform.rotation = Quaternion.Slerp(fromAngle, toAngle, Mathf.SmoothStep(0, 1, t));
            yield return null;
        }
        Striker striker = FindObjectOfType<Striker>();
        striker.MoveToLinePosition();
    }
}
